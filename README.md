# Overview

This repostiory is used to deploy the Poseoff Game demonstration on a ConfigSync enabled GDC Connected (Server or Software-Only) or GKE cluster

# Steps to Use

1. Have a cluster running with ConfigSync enabled
1. Have a cluster with External Secrets installed and configured to your GCP project (suggest appling the Cluster Trait Repo: https://gitlab.com/gcp-solutions-public/retail-edge/available-cluster-traits/external-secrets-anthos )
1. Create a Personal Access Token for your SCM provider
1. Use a Primary Root Repository for your edge cluster
1. Setup the Configuration (see /config-manifests) in the namespace of the Package State Repository config files (see below)
1. Add the Configuration for the repository (sample below)

# Creating GCP Secret

```shell
export PROJECT_ID="<your gcp project id>"
export SCM_TOKEN_USER="<personal access token user>"
export SCM_TOKEN_TOKEN="<personal access token value>"

gcloud secrets create pos-git-token-creds --replication-policy="automatic" --project="${PROJECT_ID}"
echo -n "{\"token\":'${SCM_TOKEN_TOKEN}', \"username\":'${SCM_TOKEN_USER}'}"  | gcloud secrets versions add pos-git-token-creds --project="${PROJECT_ID}" --data-file=-
```

# Sample RepoSync

```yaml
apiVersion: configsync.gke.io/v1beta1
kind: RepoSync
metadata:
  name: pose-rs                               # 7 characters in length (see convention for RoleBinding)
  namespace: poseoff-inference
spec:
  sourceFormat: "unstructured"
  git:
    repo: "https://gitlab.com/gcp-solutions-public/retail-edge/gdc-demos/poseoff-game-package.git"
    branch: "main"
    dir: "/package/<cluster-or-variant-name>" # Replace the variable with the variant or cluster name
    auth: "token"
    secretRef:
      name: pose-git-creds                    # K8s secret produced by ExternalSecret below

---

apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: pose-git-creds-es
  namespace: poseoff-inference
spec:
  refreshInterval: 24h
  secretStoreRef:
    kind: ClusterSecretStore
    name: gcp-secret-store
  target:                                       # K8s secret name
    name: pose-git-creds                        # Matches the secretRef above
    creationPolicy: Owner
  data:
    - secretKey: username                       # K8s secret key name to be set inside Secret
      remoteRef:
        key: pose-git-token-creds               # GCP Secret Name (https://cloud.google.com/secret-manager/docs/creating-and-accessing-secrets)
        property: username                      # field inside GCP Secret
    - secretKey: token                          # K8s secret key name to be set inside Secret
      remoteRef:
        key: pose-git-token-creds               # GCP Secret Name (https://cloud.google.com/secret-manager/docs/creating-and-accessing-secrets)
        property: token                         # field inside GCP Secret

---

kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: pose-package-repo
  namespace: poseoff-inference
subjects:
  - kind: ServiceAccount
    name: ns-reconciler-poseoff-inference-rs-7 # k get sa -n config-management-system ( ns-reconciler-{NAMESPACE}-{REPO_SYNC_NAME}-{REPO_SYNC_NAME_LENGTH} )
    namespace: config-management-system
roleRef:
  kind: Role
  name: pose-reconciler-admin                  # Granular control over resources can be created with RBAC (Future demo will explore custom RBAC)
  apiGroup: rbac.authorization.k8s.io

---

# Role for Reconciler
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: poseoff-inference
  name: pose-reconciler-admin
rules:
- apiGroups: ["*"]
  resources: ["*"]
  verbs: ["*"]

```

# Variations

These are common variants that would be placed in each cluster since the values vary by
each cluster. Future ConfigSync functionality will account for variants directly, until
such time, variations should be added with the `RepoSync` or `RootSync` in the
`PrimaryRootRepo`


## Local Validation

Assuming `nomos` is installed (via `gcloud components install nomos`)

```
nomos vet --no-api-server-check --path config/
```

### Docker method

Using this link to find the version of nomos-docker:  https://cloud.google.com/anthos-config-management/docs/how-to/updating-private-registry#expandable-1

```
docker pull gcr.io/config-management-release/nomos:stable
docker run -it -v $(pwd):/code/ gcr.io/config-management-release/nomos:stable nomos vet --no-api-server-check --path /code/config/
```

### ConfigSync Overview

See [our documentation](https://cloud.google.com/anthos-config-management/docs/repo) for how to use each subdirectory.

## Series Links

This project is one component in a multi-component solution. The composed parts make up the game "Price a Tray".

https://gitlab.com/mike-ensor/hat-no-hat-frontend
: This is the User Experience and User Interface for the game

https://gitlab.com/mike-ensor/price-a-tray-inference-server
: This is the inference server configured to use a hat-no-hat verioned .tflite model

https://gitlab.com/mike-ensor/rtsp-to-mjpeg.git
: This project turns a RTSP stream into a MJPEG stream for consumption by the UI

https://gitlab.com/mike-ensor/light-tower-interface.git
: This project is an interface for a light tower based on X410 Web Controller Relay

https://gitlab.com/mike-ensor/hat-no-hat-package
: This is the project used to deploy to a Kubernetes project with ConfigSync installed
